<?php namespace VWIT\Swaggervel;

use Illuminate\Support\ServiceProvider;
use VWIT\Swaggervel\InstallerCommand;

use Config;

class SwaggervelServiceProvider extends ServiceProvider {
	
	
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->package('vwit/swaggervel');

        $this->commands(array('VWIT\Swaggervel\InstallerCommand'));

        require_once __DIR__ .'/routes.php';
    }

}