<?php
header('Access-Control-Allow-Origin: *');

header('Access-Control-Allow-Methods: GET, POST');

header("Access-Control-Allow-Headers: X-Requested-With");
?>
<html>
<head>
    {{ HTML::style('https://fonts.googleapis.com/css?family=Droid+Sans:400,700'); }}
    {{ HTML::style('packages/vwit/swaggervel/css/reset.css', array('media' => 'screen'), $secure); }}
    {{ HTML::style('packages/vwit/swaggervel/css/reset.css', array('media' => 'print'), $secure); }}
    {{ HTML::style('packages/vwit/swaggervel/css/screen.css', array('media' => 'screen'), $secure); }}
    {{ HTML::style('packages/vwit/swaggervel/css/screen.css', array('media' => 'print'), $secure); }}

    {{ HTML::script('packages/vwit/swaggervel/lib/shred.bundle.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/jquery-1.8.0.min.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/jquery.slideto.min.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/jquery.wiggle.min.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/jquery.ba-bbq.min.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/handlebars-1.0.0.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/underscore-min.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/backbone-min.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/swagger.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/swagger-ui.js', array(), $secure); }}
    {{ HTML::script('packages/vwit/swaggervel/lib/highlight.7.3.pack.js', array(), $secure); }}

    <!-- enabling this will enable oauth2 implicit scope support -->
    {{--    {{ HTML::script('packages/vwit/swaggervel/lib/swagger-oauth.js' , array(), $secure); }}--}}

    <script type="text/javascript">
        $(function () {
            window.swaggerUi = new SwaggerUi({
                url: "{{{ $urlToDocs }}}",
                dom_id: "swagger-ui-container",
                supportedSubmitMethods: ['get', 'post', 'put', 'delete'],
                onComplete: function (swaggerApi, swaggerUi) {
                    log("Loaded SwaggerUI");
                    @if(isset($requestHeaders))
                        @foreach($requestHeaders as $requestKey => $requestValue)
                        window.authorizations.add("{{$requestKey}}", new ApiKeyAuthorization("{{$requestKey}}", "{{$requestValue}}", "header"));
                        @endforeach
                    @endif
                    if (typeof initOAuth == "function") {
                        /*
                         initOAuth({
                         clientId: "your-client-id",
                         realm: "your-realms",
                         appName: "your-app-name"
                         });
                         */
                    }
                    $('pre code').each(function (i, e) {
                        hljs.highlightBlock(e)
                    });
                },
                onFailure: function (data) {
                    log("Unable to Load SwaggerUI");
                },
                docExpansion: "none"
            });

            $('#input_apiKey').change(function () {
                var key = $('#input_apiKey')[0].value;
                log("key: " + key);
                if (key && key.trim() != "") {
                    log("added key " + key);
                    window.authorizations.add("key", new ApiKeyAuthorization("{{Config::get('swaggervel::app.api-key')}}", key, "query"));
                } else {
                    window.authorizations.remove("key");
                }
            })
            window.swaggerUi.load();
        });
    </script>
</head>
<body class="swagger-section">
<div id='header'>
    <div class="swagger-ui-wrap">
       
<span style="display:inline-block;float:left;margin-top:3px;font-size:20px;font-weight:bold">E-learning wizard System API Documentation</span>
        <form id='api_selector'>
           
            <div class='input'><input placeholder="http://example.com/api" id="input_baseUrl" name="baseUrl"
                                      type="text"/></div>
         
            <div class='input'><a id="explore" href="#">Explore</a></div>
        </form>
    </div>
    &nbsp;<br>&nbsp;
</div>

<div id="message-bar" class="swagger-ui-wrap">&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap"></div>
</body>
</html>
